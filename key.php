<?php
function sign($method, $url, $data, $consumerSecret, $tokenSecret)
{
    $url = urlEncodeAsZend($url);

    $data = urlEncodeAsZend(http_build_query($data, '', '&'));
    $data = implode('&', [$method, $url, $data]);

    $secret = implode('&', [$consumerSecret, $tokenSecret]);

    return base64_encode(hash_hmac('sha1', $data, $secret, true));
}

function urlEncodeAsZend($value)
{
    $encoded = rawurlencode($value);
    $encoded = str_replace('%7E', '~', $encoded);
    return $encoded;
}

// REPLACE WITH YOUR ACTUAL DATA OBTAINED WHILE CREATING NEW INTEGRATION
$consumerKey = '38gekbg4rasbjwo9tqsipei3sslpdaay';
$consumerSecret = 'w0o5o4ku09yi0dpxpeb53lngnjm2wtkd';
$accessToken = 'w6buw2iq91qiumdfxsc239loo5sfzn9a';
$accessTokenSecret = 'whm3o37yhu6ppbr4vyhijdy2u8zrnmo2';

$method = 'GET';
$url = 'https://dev.shopiaz.net/index.php/rest/V1/products/MY1';

//
$data = [
    'oauth_consumer_key' => $consumerKey,
    'oauth_nonce' => md5(uniqid(rand(), true)),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_timestamp' => time(),
    'oauth_token' => $accessToken,
    'oauth_version' => '1.0',
];

$data['oauth_signature'] = sign($method, $url, $data, $consumerSecret, $accessTokenSecret);

$curl = curl_init();

curl_setopt_array($curl, [
   CURLOPT_RETURNTRANSFER => 1,
   CURLOPT_URL => $url,
    CURLOPT_HTTPHEADER => [
        'Authorization: OAuth ' . http_build_query($data, '', ',')
    ]
]);


$result = curl_exec($curl);

echo '<pre>';
var_dump($result);
echo '</pre>';