<?php

//SET URL
$url = "";

//POST DATA ARRAY
$data = array(
    'KEY'	 		=> 'VALUE'
);

//USER / PASS - BASIC AUTH
$username = 'USER';
$password = 'PASS';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);

//IF BASIC AUTH
curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);

//IF DELETE / PUT
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

//IF POST
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

//EXECUTE
$output = curl_exec($ch);
$info = curl_getinfo($ch);

$header_size = $info['header_size'];
$header = substr($output, 0, $header_size);
$body = substr($output, $header_size);

curl_close($ch);

echo '<pre>';
	
	//CURL INFO
	echo 'INFO<br>';
	print_r($info);

	echo '<hr>';

	//CURL HEADER
	echo 'HEADER<br>';
	print_r($header);

	echo '<hr>';

	//CURL BODY
	echo 'BODY<br>';
	print_r(json_decode($body));

	echo '<hr>';

	//OUTPUT
	echo 'OUTPUT<br>';
	echo ($output);

	

echo '</pre>';

